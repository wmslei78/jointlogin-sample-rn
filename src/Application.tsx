import * as React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    EmitterSubscription
} from 'react-native';
import * as RNWeixin from '@wmslei78/react-native-weixin';
import Member from './models/Member';
import ApiClient from './repositories/ApiClient';
import MemberRepository from './repositories/MemberRepository';

export default class Application extends React.Component<{}, {
    member: Member;
}> {
    private onErrorSubscription_: EmitterSubscription = undefined;
    private onAuthSuccessSubscription_: EmitterSubscription = undefined;

    private memberRepository_ : MemberRepository = undefined;

    constructor( props: {}, context: any ) {
        super( props, context );
        this.state = {
            member: undefined
        };
        this.memberRepository_ = new MemberRepository(new ApiClient('http://192.168.199.236:8080'));
    }

    componentWillMount() {
        RNWeixin.register('wxd930ea5d5a258f4f').
        then(()=>{
            console.warn('register success');
            if (this.onErrorSubscription_) this.onErrorSubscription_.remove();
            this.onErrorSubscription_ = RNWeixin.WXEventEmitter.addListener('error', (error)=>{
                console.warn(`${error.code}==${error.description}`);
            });
            if (this.onAuthSuccessSubscription_) this.onAuthSuccessSubscription_.remove();
            this.onAuthSuccessSubscription_ = RNWeixin.WXEventEmitter.addListener('authSuccess', (authed)=>{
		        console.warn(authed.code);
                this.memberRepository_.
                login('weixin', authed.code).
                then(()=>this.profile());
            });
        }).
        catch(()=>console.warn('register no'));

        this.profile();
    }

    componentWillUnmount() {
        if (this.onErrorSubscription_) this.onErrorSubscription_.remove();
        if (this.onAuthSuccessSubscription_) this.onAuthSuccessSubscription_.remove();
        RNWeixin.destroy();
    }

    private profile() {
        this.memberRepository_.
        profile().
        then((data)=>this.setState({member: data})).
        catch((e)=>{});
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.member ? (
                        <View>
                            <Image source={ this.state.member.portrait && { uri: this.state.member.portrait } }
                                   style={ styles.portrait } />
                            <Text style={styles.weixin_text}>
                                { this.state.member.nickname }
                            </Text>
                            <TouchableOpacity style={ styles.weixin_btn }
                                              onPress={ ()=>{
                                                  this.memberRepository_.logout().then(()=>this.setState({ member: undefined }));
                                              } }>
                                <Text style={styles.weixin_text}>
                                    登出
                                </Text>
                            </TouchableOpacity>
                        </View>
                    ) : (
                        <TouchableOpacity style={ styles.weixin_btn }
                                          onPress={ ()=>{
                                              RNWeixin.auth('snsapi_userinfo', 'jointlogin').
                                              then(()=>console.warn('auth yes')).
                                              catch(()=>console.warn('auth no'));
                                          } }>
                        <Text style={styles.weixin_text}>
                            微信登录
                        </Text>
                    </TouchableOpacity>
                    )
                }

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    weixin_btn: {
        borderWidth: .5,
        borderColor: '#00ff00',
        borderRadius: 5,
    },
    weixin_text: {
        fontSize: 20,
        textAlign: 'center',
        lineHeight: 50,
        paddingHorizontal: 20,
        color: '#00ff00',
    },
    portrait: {
        width: 100,
        height: 100,
    }
});
