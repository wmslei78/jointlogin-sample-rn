import * as _ from 'underscore';


export default class ApiClient {

    private prepare( params: { [key: string]: string|number } ): string {
        return _.compact( _.map( params, ( v, k ) =>
                                 !_.isUndefined(v) && !_.isNull(v) && [
                                     encodeURIComponent(k),
                                     encodeURIComponent('' + v)
                                 ].join( '=' ) ) ).join( '&' );
    }

    private prepareUrl( url: string, params?: { [key: string]: string|number } ): string {
        if ( !params )
            return url;

        return url + '?' + this.prepare( params );
    }

    constructor( private url: string, private onForbidden?: () => any ) {
    }

    get( path: string, params?: { [key: string]: string|number } ): Promise<any> {
        return this.execute( 'GET', this.prepareUrl( path, params ) );
    }

    post( path: string, params?: { [key: string]: string|number },
          onForbidden?: () => any ): Promise<any> {
              return this.execute( 'POST', path, params, onForbidden );
          }

    postForm( path: string, form: FormData ): Promise<any> {
        return (fetch(`${this.url}${path}`, { method: 'POST', body: form }).
                then(( response ) => {
                    if ( response.ok )
                        return response.json();

                    switch ( response.status ) {
                    case 403:
                        if ( this.onForbidden )
                            return this.onForbidden();

                    default:
                        throw response.json();
                    }
                }));
    }

    put( path: string, params?: { [key: string]: string|number } ): Promise<any> {
        return this.execute( 'PUT', path, params );
    }

    delete( path: string ): Promise<any> {
        return this.execute( 'DELETE', path );
    }

    private execute(
        method: 'GET'|'POST'|'PUT'|'DELETE',
        path: string,
        params?: { [key: string]: string|number },
        onForbidden?: () => any
    ): Promise<any> {
        return (fetch(`${this.url}${path}`,
            method === 'GET' ?
                {
                    credentials: 'include',
                    method: method
                } :
                {
                    credentials: 'include',
                    method: method,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: params && this.prepare( params )
                }).
                then(( response ) => {
                    if ( response.ok )
                        return method !== 'DELETE' && response.json() || null;

                    switch ( response.status ) {
                    case 403:
                        onForbidden = onForbidden || this.onForbidden;
                        if ( onForbidden )
                            return onForbidden();

                    case 404:
                        return undefined;

                    default:
                        return new Promise((resolv, reject) => response.json().then(reject));
                    }
                }));
    }

}
