import Member from '../models/Member';
import ApiClient from './ApiClient';


export default class MemberRepository {

    constructor( private client: ApiClient ) {
    }

    profile(): Promise<Member> {
        return (
            this.client.
                get( '/profile' ).
                then( Member.cook ));
    }

    login( platform: 'weixin'|'weibo', token: string ): Promise<Member> {
        return (
            this.client.
                post(
                    `/login/${platform}`,
                    { token: token,
                    },
                    () => { throw ({ status: 403 }) }));
    }

    logout(): Promise<{}> {
        return this.client.get( '/logout' ).catch( () => {} );
    }

}
