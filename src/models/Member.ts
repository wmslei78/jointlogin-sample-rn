
type Member = Member.MemberT;

namespace Member {

    export class MemberT {

        constructor( public id: string,
                     public nickname?: string,
                     public portrait?: string ) {
        }

    }

    export function cook( data: any ): Member {
        return (data && new MemberT(
            data.id,
            data.nickname,
            data.portrait));
    }

}

export default Member;
